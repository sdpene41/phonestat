import React from "react";
import { IonList, IonItem } from "@ionic/react";

interface Props {
  data: {
    text: string;
    created_at: string;
  }[]
}


const Liste = (props: Props) => (
  <>
    {props.data ? (
      <IonList>
        {props.data
          .map((element) => (
            <IonItem>
              {element.text} ({element.created_at.substr(0,10)})
            </IonItem>
          ))}
      </IonList>
    ) : (
      <div>Chargement en cours</div>
    )}
  </>
);

export default Liste;
