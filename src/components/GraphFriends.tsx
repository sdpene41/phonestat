import React from "react";
import Chart from "react-apexcharts";

interface Props {
    options: {
        chart: {
          id: string
        },
        xaxis: {
          categories: string[]
        }
      },
      series: Array<
        {
          name: string,
          data: any[]
        }
      >
}

const GraphFriends = (props: Props) =>  (
    <>
        <div className="app">
        <div className="row">
          <div className="mixed-chart">
            <Chart
              options={props.options}
              series={props.series}
              type="line"
              width="100%"
              height="500"
            />
          </div>
        </div>
      </div>
    </>
);

export default GraphFriends;